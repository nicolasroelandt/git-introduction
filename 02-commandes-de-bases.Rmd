# Utiliser Git

## Fonctionnement

*Git* fonctionne avec un système de dépôts.

- le dépôt local sur l'ordinateur de l'utilisateur: c'est la copie de travail
- le dépôt distant de l'utilisateur: *origin*
- le dépôt collaboratif: *upstream*

### Utilisateur unique

![Git avec un utilisateur unique](images/git_simple_repo.jpg)

### Mode collaboratif

![Git en mode collaboratif](images/git_usual_workflow.jpg)

## Usage
### Ligne de commande

Git a été conçu pour être utilisé avec la ligne de commande et si de nombreux outils permettent de s'en passer, voyons les différentes étapes de travail

#### `git init`

Cette commande créé un dossier `.git` dans le répertoire courant, caché, où sera stocké l'historique git.

Si vous supprimez ce dossier `.git`, l'historique local sera perdu.

#### `git clone`

Permet de cloner un dépôt distant dans le répertoire courant à partir d'une url.

```bash
git clone [url]
```

Par exemple, pour cloner ce projet vous pouvez utiliser:

```bash
git clone https://gitlab.com/nicolasroelandt/git-introduction.git
```

#### `git diff`

Permet de visualiser la différence entre 2 *commits*.

- Pour voir la différence entre le dernier *commit* et l'état du répertoire courant

```bash
git diff
```

- Pour voir la différence entre le dernier *commit* et l'état du répertoire courant

```bash
git diff [hash du premier commit] [hash du second commit]
```

#### `git status`

Permet d'afficher le status des fichiers.
On peut distinguer 3 types de statuts:

- non suivi
- suivi
- *staged*


```{r fig.cap="3 états (source: git-scm.com)" ,results = 'asis', echo=FALSE}
download.file(url = "https://git-scm.com/book/en/v2/images/areas.png",
          destfile = "image.png",
          mode = 'wb')
knitr::include_graphics(path = "image.png")
```

### `git add`

Ajoute (indexe) un ou plusieurs fichier(s) non suivi(s) ou suivi(s) à la liste des fichiers qui seront commités.

Les modifications  effectuées dans les fichiers de la liste seront enregistrées dans le prochain *commit*. 
Les autres fichiers seront ignorés. Cela permet de créer plusieurs *commits* atomiques.

Les fichiers non suivis seront suivis par la suite.

```bash
git add [nom du fichier]
```


Exemples :

- Pour ajouter un fichier 

```bash
git add toto.txt
```

- Pour ajouter plusieurs fichiers

```bash
git add toto.txt titi.md
```


- Pour ajouter tous les fichiers modifiés 

```bash
git add .
```

#### `git commit`

Commande permettant d'enregister des changements dans l'historique.

La commande `git commit` attend *toujours* un message court détaillant les changements effectués. Seuls les fichiers *staged* seront enregistrés dans le *commit*.


```bash
git commit -m "Exemple de commit"
```


#### `git push`

Permet de **pousser** vers un dépôt distant.

```bash
git push [dépôt] [branche]
```

- pour pousser sur la branche *master* :

```bash
git push origin master
```

- pour pousser sur une autre branche

```bash
git push origin pourquoi_versionner
```

Pour pouvoir pousser sur un dépôt, il faut avoir le droit en écriture. Sinon, il faut faire une demande (*Merge*/*Pull* *request*) depuis son compte distant *origin* vers le dépôt collaboratif *upstream*. 

#### `git pull`

Permet de récuperer localement la dernière version déposée en ligne.

```bash
git pull [dépot] [branche]
```

- depuis son dépôt personnel : `git pull origin master`
- depuis le dépôt collaboratif: `git pull upstream master`

![Git en mode collaboratif](images/git_usual_workflow.jpg)

#### `git remote`

Permet de lister les dépots distants.

```bash
git remote -v
```
L'option `-v` (*verbose*) permet d'avoir les URLs.

Il y a plusieurs types de dépôts distants

- `origin` : votre dépôt distant
- `upstream` : celui du projet ou du développeur principal
- les dépôts distants des autres développeurs qui contribuent au projet. Ils ne sont pas ajoutés automatiquement, il faut les ajouter.

#### `git checkout`

*Git* fonctionne avec une hiérachie sous forme d'arbre.

Le tronc est la branchée appelée *master* par convention.
On peut créer autant de branches et sous-branches qu'on le souhaite.

Il existe plusieurs manières de travailler avec les branches mais la branche *master* sert à stocker les nouvelles versions stables. Des sous-branches pour chaque version en développement sont créées, une fois suffisamment matures, elles sont fusionnées avec *master*.

Le fait de créer des branches et sous-branches permet de travailler collaborativement sur un dépot, les changements n'intervenant qu'au moment de la fusion.
Les conflits seront alors réglés à ce moment-là.

##### Créer une nouvelle branche

```bash
git checkout -b [nouvelle-branche]
```

Les noms de branches doivent être séparés par des '_' ou des '-', pas d'espaces.

Par exemple :

```bash
git checkout -b pourquoi_versionner
```

L'option `-b` permet de créer et basculer automatiquement sur la nouvelle branche.

##### Changer de branche

```bash
git checkout [branche]
```

Par exemple, pour revenir sur la branche *master* :

```bash
git checkout master
```

##### Lister les branches

```bash
git branch -l
```

## En dernier recours

```{r fig.cap="Conseils précieux", results = 'asis', echo=FALSE}
download.file(url = "https://imgs.xkcd.com/comics/git.png",
          destfile = "image_xkcd.png",
          mode = 'wb')
knitr::include_graphics(path = "image_xkcd.png")
```

Source: [xkcd Comics](https://xkcd.com/1597/)

### Clients applicatifs

L'interface en ligne de commande n'étant pas forcément pratique pour tous, il existe des clients applicatifs.

Ils ont généralement l'avantage de représenter l'arbre de dévelopement et ses différentes branches.

- [GitKraken](https://www.gitkraken.com/)
- [SourceTree](https://www.sourcetreeapp.com/)
- [git-cola](https://git-cola.github.io/)
- [TortoiseGit](https://tortoisegit.org/)

### Modules intégrés

De nombreux logiciels fournissent des modules intégrés pour le versionnement avec Git, par exemple :

- VS Code
- Rstudio
- Eclipse

