html:
	Rscript -e "bookdown::render_book('index.Rmd')"

pdf:
	Rscript -e "bookdown::render_book('index.rmd', output_format = 'bookdown::pdf_book')"

epub:
	Rscript -e "bookdown::render_book('index.rmd', output_format = 'bookdown::epub_book')"

all: clean html pdf epub

clean:
	rm -rf _book/*
	rmdir _book/
