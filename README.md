This document is an introduction to versionning using the git tool, in French.

## About

This book has been made by [Nicolas Roelandt](https://roelandtn.frama.io/) ( [Uni. Gustave Eiffel/AME](https://www.univ-gustave-eiffel.fr/recherche/composantes-de-recherche/)) using R Markdown and **bookdown** (https://github.com/rstudio/bookdown). Please see the page "Get Started" at https://bookdown.org/home/about/ for how to compile this example.

## Licence

This document is under [Creatives Commons CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) licence.

