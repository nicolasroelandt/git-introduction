# Pourquoi versionner ?

Le versionnement permet de sauvegarder l'état d'un dossier et d'en créer une photographie à un instant donné.
Il est possible de revenir à un état enregistré (*commit*).

Le versionnement permet de garder un historique des modifications faites sur les fichiers sources (fini les `totoV1.txt`, `totoV2.txt`, `totoV2_final.txt`, `totoV2_final2.txt`).

Il permet aussi le travail collaboratif en permettant un travail en local et en partageant les avancées (sans saturer les boîtes mail).

Chaque *commit* porte un message expliquant les modifications qu'il apporte et apporte de l'information résumée sur ces modifications.

De plus, recourir à un dépôt distant permet:

- créer une sauvegarde distante
- échanger des modifications si l'on doit travailler avec plusieurs ordinateurs (fini les clés USB !)


## Qu'est-ce qu'on peut versionner ?

Principalement des fichiers textes :

- documents plats (`.txt`, `.md`, `.csv`, `.html`)
- fichiers de code source (`.java`, `.R`, `.py`, `.tex`)

Pour les fichiers binaires, c'est plus compliqué.
Git ne peut pas les lire mais peut en faire une copie à l'instant *t*.
Ces copies s'accumulent avec le temps et rendre le dépôt inutilement volumineux.

Idem pour les jeux de données, même en `.csv` qui peuvent alourdir rapidement un dépôt.

On préfèra versionner les fichiers sources et ne pas versionner les fichiers produits.

Les données, si elles sont volumineuses, gagneront à être déposées sur une autre plateforme ( [data.gouv.fr](https://www.data.gouv.fr/fr/) par exemple).

Il est possible de configurer *Git* pour **ignorer** certains fichiers ou dossiers. 

## Pourquoi choisir *Git*

- Très largement utilisé dans les communautés libres et open-source
- Beaucoup de ressources en ligne
- Grande puissance
- Distribué
- Compatible avec de nombreux services de dépots

C'est un logiciel libre créé en 2005 par [Linus Torvalds](https://www.wikiwand.com/en/Linus_Torvalds) lassé des problèmes qu'il rencontrait avec [Bitkeeper](https://www.bitkeeper.org/) (alors non libre) au cours du développement du noyau linux.

Il existe d'autres systèmes de versionnement ( [Subversion](https://subversion.apache.org/), [Mercurial](https://www.mercurial-scm.org/), [Bazaar](https://help.ubuntu.com/lts/serverguide/bazaar.html)) mais la puissance de *Git* et le fait qu'il soit utilisé pour le noyau linux a largement contribué à son succès.

## Où déposer son code ?

- en local
- en ligne :
  - [GitHub](https://github.com/)
  - [Bitbucket](https://bitbucket.org/)
  - [Gitlab](https://about.gitlab.com/)
  - Instances privées : [Framagit](https://framagit.org/public/projects), [git.osgeo.org](https://git.osgeo.org/gitea/)
  
Feue l'IFSTTAR disposait d'une instance Gitlab privée, inaccessible actuellement.




